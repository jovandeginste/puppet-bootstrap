#!/bin/bash

set -eu
set -o pipefail

source /etc/os-release
REPOSITORY_ROOT=/var/lib/puppet-deployment

cat <<EOF
Detected OS: '$NAME'

This script will:
* install puppet
* checkout the Puppet manifests found on Github
* apply that configuration to this system
* periodically update the git repository and apply

Wait 10 seconds to continue the deployment.
Press ctrl+c to abort now.
EOF

sleep 10

if [[ "$UID" != "0" ]]; then
  cat <<EOF
This script requires superuser access. You should rerun it as:
$ sudo $0 ${@@Q}
EOF
  exit 1
fi

case "$NAME" in
  Fedora)
    PUPPET_PACKAGE="puppet-agent"
    ;;
  Ubuntu)
    PUPPET_PACKAGE="puppet-agent librarian-puppet"
    ;;
  "Raspbian GNU/Linux")
    PUPPET_PACKAGE="puppet librarian-puppet"
    ;;
esac

case "$NAME" in
  Fedora)
    dnf -y update

    if ! command -v puppet >/dev/null; then
      echo "Puppet not yet installed - installing now..."
      if ! rpm -q puppet-release; then
        dnf -y install https://yum.puppetlabs.com/puppet-release-fedora-30.noarch.rpm
      fi
      dnf -y install ${PUPPET_PACKAGE} git-core
    fi
    if ! command -v ruby >/dev/null; then
      dnf -y install ruby
    fi
    GEM_INSTALL_PARAMS="--no-document"
    ;;
  Ubuntu | "Raspbian GNU/Linux")
    if systemctl is-active -q unattended-upgrades.service; then
      systemctl stop unattended-upgrades
    fi

    if ! command -v puppet >/dev/null; then
      echo "Puppet not yet installed - installing now..."
      if ! dpkg -l puppet-release; then
        curl -sSL https://apt.puppetlabs.com/puppet-release-$(lsb_release -cs).deb >/tmp/puppet-release.deb
        dpkg -i /tmp/puppet-release.deb
        rm -f /tmp/puppet-release.deb
        apt update
      fi
      apt -y install ${PUPPET_PACKAGE} git-core
    fi
    if ! command -v ruby >/dev/null; then
      apt -y install ruby
    fi
    GEM_INSTALL_PARAMS="--no-document"
    ;;
  *)
    echo "Unknown/unsupported operating system. Bailing out." >&2
    exit 1
    ;;
esac

if ! command -v librarian-puppet >/dev/null; then
  echo "Librarian-puppet not yet installed - installing now..."
  gem install $GEM_INSTALL_PARAMS librarian-puppet
fi

for domain in gitlab.com github.com; do
  if ! grep -q "$domain" /etc/ssh/ssh_known_hosts; then
    ssh-keyscan "$domain" >>/etc/ssh/ssh_known_hosts
  fi
done

if [[ ! -d "${REPOSITORY_ROOT}" ]]; then
  ssh-agent /bin/bash <<EOF
ssh-add /root/.ssh/puppet
git clone --quiet --depth 1 git@gitlab.com:jovandeginste/puppet-os-config.git "${REPOSITORY_ROOT}"
EOF
fi

export FACTER_deploy=true

${REPOSITORY_ROOT}/puppet-apply.sh --tags early

case "$NAME" in
  Ubuntu | "Raspbian GNU/Linux")
    apt update
    ;;
esac

${REPOSITORY_ROOT}/puppet-apply.sh
rmdir /.deploy

cat <<EOF
You should reboot now.
EOF
